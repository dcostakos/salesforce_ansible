# salesforce_ansible

Salesforce + Ansible Integration.  Note: this is SUPER beta.

To dos:

- Doc how to get a session_id for use in your environment
- Doc how to setup a virtualenv with simple_salesforce installed with ansible so this playbook can be run
- Or should I package simple_salesforce and this module for convenience
- Get feedback on activity logging via OracleAccountNumber__c ... is this how we should do it?  It's messy when done by name.

Example output from sample playbook:

```
(venv) Daves-MacBook-Pro:ansible dcostako$ ansible-playbook presales_test.yaml
[WARNING]: You are running the development version of Ansible. You should only run Ansible from "devel" if you are
modifying the Ansible engine, or trying out features under development. This is a rapidly changing source of code and
can become unstable at any point.
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match
'all'

PLAY [Test the presales sfdc module] *********************************************************************************

TASK [Gathering Facts] ***********************************************************************************************
ok: [localhost]

TASK [get technical win date on opportunity] *************************************************************************
ok: [localhost]

TASK [debug] *********************************************************************************************************
ok: [localhost] => {
    "results": {
        "changed": false,
        "failed": false,
        "message": "Technical Win for Opportunity 4131472 on date 2020-10-06",
        "opportunity_number": "4131472",
        "success": true,
        "technical_win_date": "2020-10-06"
    }
}

TASK [set technical win date on opportunity] *************************************************************************
changed: [localhost]

TASK [debug] *********************************************************************************************************
ok: [localhost] => {
    "results": {
        "changed": true,
        "failed": false,
        "message": "Set technical win date on 4131472 to 2020-10-06",
        "opportunity_id": "0063a00000iEWjSAAW",
        "opportunity_number": "4131472",
        "success": true,
        "technical_win_date": "2020-10-06"
    }
}

TASK [set a bad date on a technical win for an opportunity] **********************************************************
fatal: [localhost]: FAILED! => {"changed": false, "message": "Invalid Technical Win String 06-10-2020", "success": false}
...ignoring

TASK [debug] *********************************************************************************************************
ok: [localhost] => {
    "results": {
        "changed": false,
        "failed": true,
        "message": "Invalid Technical Win String 06-10-2020",
        "success": false
    }
}

TASK [log a Salesforce activity to an opportunity] *******************************************************************
changed: [localhost]

TASK [debug] *********************************************************************************************************
ok: [localhost] => {
    "result": {
        "activity": "00T3a000053c39ZEAQ",
        "changed": true,
        "failed": false,
        "message": "Created activity 00T3a000053c39ZEAQ",
        "query": "SELECT Id FROM Opportunity WHERE OpportunityNumber__c = '4131472'",
        "success": true,
        "user_id": "00560000002S4l3AAC"
    }
}

TASK [log a Salesforce activity to an account] ***********************************************************************
changed: [localhost]

TASK [debug] *********************************************************************************************************
ok: [localhost] => {
    "result": {
        "account_id": "0013000000A4I2MAAV",
        "account_query": "Select Id, Name from Account WHERE OracleAccountNumber__c = '1042803' AND Owner.Region__c = 'NA' AND Owner.UserRegion__c = 'West'",
        "activity": "00T3a000053bsUbEAI",
        "changed": true,
        "failed": false,
        "message": "Created activity 00T3a000053bsUbEAI",
        "success": true
    }
}

PLAY RECAP ***********************************************************************************************************
localhost                  : ok=11   changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=1
```
