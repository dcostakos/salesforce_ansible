#!/usr/bin/python

# Copyright: (c) 2020 David Costakos <dcostako@redhat.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: presales_sfdc

short_description: Module for querying and setting the technical win field in Salesforce

version_added: "0.0.1"

description: Module for setting and querying the Technical Win Date field in Salesforce

options:
    instance:
        description: URL for the Instace of Salesforce being accessed
        required: True
        type: str
    session_id:
        description: Session ID from a logged in Salesforce session
        required: True
        type: str
    opportunity_number:
        description: Opportunity Number to Execute Against
        required: False
        type: str
    technical_win_date:
        description: Date to mark in the technical win field of an opportunity (required opportunity_number fied to be present).  Date should be in YYYY-MM-DD format
        requirued: False
        type: str
    oracle_account_number:
        description: The unique oracle account number for an account to log activity aginst
        required: False
        type: str
    account_region_filter:
        description:  Match accounts within this Region (a region is inside a GEO)
        required: False
        type: str
    account_geo_filter:
        description: Match accounts within this GEO
        required: False
        type: str
    activity_type:
        description: Type of activity to log.  Should be in {"PoC", "Deep Dive", "Demo", "Discovery Session", "Workshop", "Technical Proposal", "RFP"}
        required: False
        type: str
    activity_user_name:
        description: The Salesforce name of the user the activity should be logged under
        required: False
        type: str

author:
    - Dave Costakos (dcostako@redhat.com)
'''

EXAMPLES = r'''
-- TBD ---
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
-- tbd --
'''

from ansible.module_utils.basic import AnsibleModule
from simple_salesforce import Salesforce, SalesforceError
from datetime import datetime

def validate_date_string(datestring):
    if not datestring:
        return True

    format="%Y-%M-%d"
    try:
        datetime.strptime(datestring, format)
        return True
    except ValueError:
        return False

def find_opportunity(sf, num):
    data = sf.query("SELECT Id, Technical_Win_Date__c FROM Opportunity WHERE OpportunityNumber__c = '{}'".format(num))
    if data['records']:
        return (data['records'][0]['Id'], data['records'][0]['Technical_Win_Date__c'])
    else:
        return (None, None)

def find_account(sf, num):
    data = sf.query("".format(num))
    if data['records']:
        return (data['records'][0]['Id'])
    else:
        return (None, None, None)

def get_technical_win_date(sf, params, result):
    data = sf.query("SELECT Id, Technical_Win_Date__c FROM Opportunity WHERE OpportunityNumber__c = '{}'".format(params['opportunity_number']))

    if data['records']:
        result['changed'] = False
        result['success'] = True
        result['technical_win_date'] = data['records'][0]['Technical_Win_Date__c']
        result['opportunity_number'] = params['opportunity_number']
        result['message'] = "Technical Win for Opportunity {} on date {}".format(params['opportunity_number'], data['records'][0]['Technical_Win_Date__c'])
        return (data['records'][0]['Id'], data['records'][0]['Technical_Win_Date__c'])
    else:
        return (None, None)

def set_technical_win_date(sf, params, result):
    if not validate_date_string(params['technical_win_date']):
        result['changed'] = False
        result['success'] = False
        result['failed'] = True
        result['message'] = "Invalid date, must be in YYYY-MM-DD format, received '{}'".format(params['technical_win_date'])
        return None

    data = sf.query("SELECT Id, Technical_Win_Date__c FROM Opportunity WHERE OpportunityNumber__c = '{}'".format(params['opportunity_number']))

    if data['records']:
        sf.Opportunity.update(data['records'][0]['Id'], {'Technical_Win_Date__c': params['technical_win_date']})
        result['success'] = True
        result['changed'] = True
        result['opportunity_number'] = params['opportunity_number']
        result['technical_win_date'] = params['technical_win_date']
        result['message'] = "Set technical win date on {} to {}".format(params['opportunity_number'], params['technical_win_date'])
        result['opportunity_id'] = data['records'][0]['Id']
        return None
    else:
        result['success'] = False
        result['failed'] = True
        result['changed'] = False
        result['message'] = "Unable to find opportunity by number {}".format(params['opportunity_number'])

    return None

def log_activity_to_opp(sf, params, result):
    # find user
    user = sf.query("SELECT Id from User where Name = '{}'".format(params['activity_user_name']))
    if not user['records']:
        result['success'] = False
        result['failed'] = True
        result['changed'] = False
        result['message'] = "Can't locate user with name '{}'".format(aprams['activity_user_name'])
        return None
    
    user_id = user['records'][0]['Id']

    # Get opp id
    query = "SELECT Id FROM Opportunity WHERE OpportunityNumber__c = '{}'".format(params['opportunity_number'])
    data = sf.query(query)
    if data['records']:
        subject = "{} on {} by created Ansible on {}".format(params['activity_type'], params['opportunity_number'], datetime.now())
        id = data['records'][0]['Id']
        # insert activity and handle errors
        insert_response = sf.Task.create({'WhatId': id, 'Owner__c': user_id, 'Status': "Completed", 'Subject': subject, 'Type': params['activity_type'], 'ActivityDate': params['activity_date']})
        print(insert_response)
        if insert_response['success']:
            result['success'] = True
            result['changed'] = True
            result['message'] = "Created activity {}".format(insert_response['id'])
            result['activity'] = insert_response['id']
            result['user_id']  = user_id
            result['query'] = query
        else:
            result['success'] = False
            result['changed'] = False
            result['message'] = "Failed to create activity with message: {}".format(insert_response['errors'])
        return None
    else:
        result['success'] = False
        result['failed'] = True
        result['changed'] = False
        result['message'] = "LOG ACTIVITY: Unable to find opportunity by number '{}'".format(params['opportunity_number'])
    return None

def log_activity_to_account(sf, params, result):
    # find user
    user = sf.query("SELECT Id from User where Name = '{}'".format(params['activity_user_name']))
    if not user['records']:
        result['success'] = False
        result['failed'] = True
        result['changed'] = False
        result['message'] = "Can't locate user with name '{}'".format(aprams['activity_user_name'])
        return None
    
    user_id = user['records'][0]['Id']

    # find account
    # SELECT Id, Name, Owner.NA_Subregion__c FROM Account WHERE OracleAccountNumber__c = '1547179' and Owner.Region__c = 'NA' and Owner.UserRegion__c = 'West'
    query = "Select Id, Name from Account WHERE OracleAccountNumber__c = '{}'".format(params['oracle_account_number'])
    if params['account_geo_filter']:
        query += " AND Owner.Region__c = '{}'".format(params['account_geo_filter'])
    
    if params['account_region_filter']:
        query +=  " AND Owner.UserRegion__c = '{}'".format(params['account_region_filter'])
    
    data = sf.query(query)
    id = data['records'][0]['Id']

    if data['records']:
        subject = "{} on Account {} by created Ansible on {}".format(params['activity_type'], data['records'][0]['Name'], datetime.now())
        insert_response = sf.Task.create({'WhatId': id, 'Owner__c': user_id, 'Status': "Completed", 'Subject': subject, 'Type': params['activity_type'], 'ActivityDate': params['activity_date']})
        if insert_response['success']:
            result['success'] = True
            result['changed'] = True
            result['message'] = "Created activity {}".format(insert_response['id'])
            result['activity'] = insert_response['id']
            result['account_id'] = id
            result['account_query'] = query
        else:
            result['success'] = False
            result['failed'] = True
            result['changed'] = False
            result['message'] = "Failed to create activity with message: {}".format(insert_response['errors'])
        return None
    else:
        result['success'] = False
        result['failed'] = True
        result['changed'] = False
        result['message'] = "Can't locate account via query {}".format(query)
        result['query'] = query

    return None

    

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        instance=dict(type='str', required=True),
        session_id=dict(type='str', required=True, no_log=True),
        opportunity_number=dict(type='str', required=False),
        technical_win_date=dict(type='str', required=False),
        oracle_account_number=dict(type='str', required=False),
        account_region_filter=dict(type='str', required=False),
        account_geo_filter=dict(type='str', required=False),
        activity_type=dict(type='str', required=False),
        activity_user_name=dict(type='str', required=False),
        activity_date=dict(type='str',required=False)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        success=True,
        message='Default',
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    # validate the technical win date
    if module.params['technical_win_date'] and not validate_date_string(module.params['technical_win_date']):
        result['success'] = False
        result['failed'] = True
        result['message'] = "Invalid Technical Win String {}".format(module.params['technical_win_date'])
        module.exit_json(**result)


    # login to the salesforce instance
    sf = Salesforce(instance_url=module.params['instance'], session_id=module.params['session_id'])

    try: 
        # determine what we should do based on the options entered

        # if the opportunity number is entered, either log activity aginst the opportunity or do tech win
        if module.params['opportunity_number']:
            if module.params['technical_win_date']:
                set_technical_win_date(sf, module.params, result)
            elif module.params['activity_type']:
                log_activity_to_opp(sf, module.params, result)
            else:
                get_technical_win_date(sf, module.params, result)
        elif module.params['oracle_account_number']:
            if module.params['activity_type']:
                log_activity_to_account(sf, module.params, result)
            else:
                result['success'] = False
                result['failed'] = True
                result['message'] = "No actionable activity sent"
    except SalesforceError as err:
        result['success'] = False
        result['failed'] = True
        result['changed'] = False
        result['message'] = "Error from salesforce {}".format(err)
    #(opp_id, opp_win) = find_opportunity(sf, module.params['opportunity_number'])


    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
